import { Component } from '@angular/core';

@Component({
  selector: 'app-main',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->
    <div  class="main-content">
      <h1>
       {{title}}
      </h1>

      <app-welcome>
        
      </app-welcome>
    </div>
  
    
  `,
  styles: [`
    .main-content{
        padding: 30px;
        font-family: sans-serif;
        
    }
  
  `]
})
export class AppComponent {
  title = 'Skład reprezentacji Polski : ';
}
